---
title: "0530 power up"
author: "Hao Chen"
date: "5/30/2018"
output: 
    html_document:
      theme: cosmo
      toc: true
      toc_float: false
      highlight: monochrome
      code_folding: hide
      df_print: kable
---


```{r libraries,include = FALSE, message=FALSE,warning=FALSE}
library(purrr)
library(readr)
library(dplyr)
library(ggplot2)
library(tidyr)
library(lubridate)
library(scales)
```








```{r train-test split2}
df20171231 = read_rds('df20171231.rds')
sample_size_df20171231 = floor(nrow(df20171231)*0.8)
set.seed(123)
train_ind = sample(seq_len(nrow(df20171231)),size = sample_size_df20171231)

training_df20171231 <- df20171231[train_ind,]
testing_df20171231 <- df20171231[-train_ind,]



# I want to add more 'inactive users' to training set

only_inactive <- training_df20171231 %>%
  filter(REAL_ACTIVE_IN_MARCH == '0')

new_training_df20171231 = rbind(training_df20171231,only_inactive)
new_training_df20171231 = rbind(new_training_df20171231,only_inactive)
new_training_df20171231 = rbind(new_training_df20171231,only_inactive)
new_training_df20171231 = rbind(new_training_df20171231,only_inactive)
new_training_df20171231 = rbind(new_training_df20171231,only_inactive)
new_training_df20171231 = rbind(new_training_df20171231,only_inactive)
new_training_df20171231 = rbind(new_training_df20171231,only_inactive)
new_training_df20171231 = rbind(new_training_df20171231,only_inactive)
new_training_df20171231 <- new_training_df20171231[sample(nrow(new_training_df20171231)),]

new_training_df20171231 <- new_training_df20171231 %>%
  filter(!(user_id %in% c(364,369)))

# write_rds(training_df20171231,'training_df20171231.rds')
# write_rds(testing_df20171231,'testing_df20171231.rds')
# write_rds(new_training_df20171231,'new_training_df20171231.rds')

```




### Radiant logistic, centered, stded



```{r,message=FALSE, warning=false, include=FALSE}
set.seed(123)
library(radiant.model)
radiant_logistic <- logistic(
  dataset = new_training_df20171231,
  rvar = "REAL_ACTIVE_IN_MARCH2",
  evar = c(
  "orient_info", #1
  "level_vision",#2
   "agent_description",#3
    "sub_1708", #4
    "after_1704" ,#5
    "Plan_Minutes", #6
    "Term",   #7
    "tot_min", #8
    "perc_usage", #9
    "tot_month",#10
    "discount_type", #11
    "tot_coupon", #12
    "tot_credithour",#13
    "perc_creditmin", #14
    "referrer", #15
    "referral", #16
    "connection_quality_lower_better", #17
    "average_waiting_time", #18
    "average_serving_time", #19
    "how_often_a_user_make_call_by_day", #20
    "lyft_plus_uber", #21
    "mean_agent_experience_previous_100call_number", #22
    "tot_usage_average_by_month",#23
    "tot_payment_average_by_month",#24
    "tot_coupon_by_month" #25
  ),
  lev = "ACTIVE_IN_MARCH",
  int = c("level_vision:tot_usage_average_by_month",
          "tot_coupon_by_month:connection_quality_lower_better",
          "tot_coupon:referrer","level_vision:average_serving_time", 
          "referrer:connection_quality_lower_better",
          "referrer:how_often_a_user_make_call_by_day",
          "referrer:perc_usage","referrer:average_serving_time",
          "agent_description:Plan_Minutes",
          "mean_agent_experience_previous_100call_number:average_waiting_time",
          "mean_agent_experience_previous_100call_number:level_vision",
          "Plan_Minutes:how_often_a_user_make_call_by_day",
          "tot_coupon_by_month:how_often_a_user_make_call_by_day",
          "sub_1708:tot_credithour",
          "sub_1708:how_often_a_user_make_call_by_day",
          "sub_1708:Plan_Minutes",
          "sub_1708:connection_quality_lower_better"),
 check = "standardize"
)

```
  
```{r}  
summary(radiant_logistic)
pred <- predict(radiant_logistic, pred_data = testing_df20171231)
#print(pred, n = 10)

result_of_logistic =  testing_df20171231
result_of_logistic$pred_radiant_logistic = pred$Prediction
```










```{r xi_auc}

summary(radiant_logistic)

prob_radiant_logistic <-predict(radiant_logistic, pred_data = testing_df20171231)[,'Prediction']
testing_df20171231$pred_radiant_logistic <- prob_radiant_logistic
auc_radiant_logistic <- ModelMetrics::auc(actual = ifelse(testing_df20171231$REAL_ACTIVE_IN_MARCH2 == "ACTIVE_IN_MARCH", 1, 0),predicted = prob_radiant_logistic)
cm_radiant_logistic <- caret::confusionMatrix( as.factor(ifelse(prob_radiant_logistic>0.5,1,0)), as.factor(ifelse(testing_df20171231$REAL_ACTIVE_IN_MARCH2 == "ACTIVE_IN_MARCH", 1, 0)))
cm_radiant_logistic

```




```{r fig.width = 3, fig.height = 2, dpi = 200}
result_of_logistic2 <- evalbin(
  result_of_logistic, 
  pred = "pred_radiant_logistic", 
  rvar = "REAL_ACTIVE_IN_MARCH2", 
  lev = "ACTIVE_IN_MARCH", 
  train = "All"
)
#summary(result_of_logistic2, prn = FALSE)

#plot(result_of_logistic2, plots = "gains", custom = FALSE)
```


```{r}
result_of_logistic3 <- confusion(
  result_of_logistic, 
  pred = "pred_radiant_logistic", 
  rvar = "REAL_ACTIVE_IN_MARCH2", 
  lev = "ACTIVE_IN_MARCH", 
  train = "All"
)
summary(result_of_logistic3)
```



```{r}
table(result_of_logistic$REAL_ACTIVE_IN_MARCH2,(result_of_logistic$pred_radiant_logistic>0.5))
```



```{r}
options(scipen=999)
logistic_coefficient_df <- radiant_logistic$coeff %>% 
  arrange(`p.value`) %>% 
  select(label, OR, `p.value`)
logistic_coefficient_df$prob_0.5base = -(0.5-0.5*logistic_coefficient_df$OR)/(1+logistic_coefficient_df$OR)

knitr::kable(logistic_coefficient_df)
```




